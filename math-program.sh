NUM1=$1 
NUM2=$2 
NUM3=$3 

RESULT=$((NUM1**NUM2+NUM3*NUM2)) 

if [[ ! -d /build ]] #if statements check for build folder
then
    echo "/build folder doesn't exist. Creating now"
    mkdir -p "build"
    echo "Folder created"
    else #leaves folder as is
    echo "/build folder exists"
fi

if [ -z ${OUTPUT_FILE_NAME} ] #checks environmental variables
then #creates default file
    OUTPUT_FILE_NAME="build/myresult.txt"
    echo "Using default output file name of '$OUTPUT_FILE_NAME'."
else #confirms the file name was found in environmental variables
    echo "Using given file name of '$OUTPUT_FILE_NAME'"
fi


echo "Calculating : ($NUM1 ^ $NUM2) + $NUM3 * $NUM2" 

echo "Result: $RESULT" 

echo $RESULT > $OUTPUT_FILE_NAME 
